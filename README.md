#Tamagotchi

- sbt run
- sbt test

##Commands:

####feed (f)
- increase health by 1
- decrease hunger by 1
- wakes up bahamut
####sleep (s)
- put to sleep
####clean (c)
- increase health by 1
- decrease poop by 1
####print (p)
- print bahamut status
####reset (r)
- reset bahamut
####quit (q)
- exit program

## Info
- game prints status every minute
- ticks per minute
- age increase every 5 ticks
- max age 30 (150 ticks)
- bahamut goes to sleep at multiples of 3 minutes
- hunger and poop increases by 1 on each tick
- health decrease by 1 or 2 if bahamut is hungry or unclean
