name := "tamagotchi"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.4.9",
  "org.scalacheck"    %% "scalacheck" % "1.13.2" % "test"
)

