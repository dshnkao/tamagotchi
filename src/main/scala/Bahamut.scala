class Bahamut(private var _age: Int,
              private var _health: Int,
              private var _hunger: Int,
              private var _poop: Int,
              private var _activity: Activity) {
  import Bahamut._
  require(0 <= _health && _health <= MaxNum)
  require(0 <= _hunger && _hunger <= MaxNum)
  require(0 <= _poop && _poop <= MaxNum)

  def age = _age / TicksPerAge
  def health = _health
  def hunger = _hunger
  def poop = _poop
  def activity = _activity

  override def toString: String =
    s"""
       |Bahamut(age:        $age/$MaxNum,
       |        health:     $health/$MaxNum,
       |        hunger:     $hunger/$MaxNum,
       |        poop:       $poop/$MaxNum,
       |        activity:   $activity)
       |""".stripMargin


  def update(ticks: Int, minute: Int): Unit = {
    _age      = _age + ticks
    _health   = clamp(health - starve(hunger, ticks) - sick(poop, ticks))
    _hunger   = clamp(hunger + ticks)
    _poop     = clamp(poop + ticks)
    _activity = if (isSleepTime(minute)) Asleep else Awake
  }

  def feed(): Unit = {
    _hunger = clamp(_hunger - 1)
    _health = clamp(_health + 1)
    _activity = Awake
  }

  def sleep(): Unit = _activity = Asleep

  def clean(): Unit = {
    _poop  = clamp(_poop - 1)
    _health = clamp(_health + 1)
  }

  def reset(): Unit = {
    _age = 0
    _health = MaxNum
    _hunger = MaxNum/2
    _poop = 0
    _activity = Awake
  }

  def isDead: Boolean = age > MaxNum || health <= 0

  def isAlive: Boolean = !isDead

  def copy() = new Bahamut(_age, _health, _hunger, _poop, _activity)
}

object Bahamut {

  val MaxNum = 30
  val TicksPerAge = 5
  val limit = MaxNum/2

  def apply(): Bahamut = new Bahamut(
    _age = 0,
    _health = MaxNum,
    _hunger = MaxNum/2,
    _poop = 0,
    _activity = Awake)

  def clamp(num: Int) =
    if (num > MaxNum) MaxNum
    else if (num < 0) 0
    else num

  def isSleepTime(time: Int): Boolean = if (time % 3 == 0) true else false

  def starve(hunger: Int, ticks: Int): Int = (hunger + ticks - limit) min ticks max 0

  def sick(poop: Int, ticks: Int): Int = (poop + ticks - limit) min ticks max 0
}

sealed abstract class Activity
case object Asleep extends Activity
case object Awake extends Activity
