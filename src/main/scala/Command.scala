
object Command extends Enumeration {
  type Command = Value
  val Feed = Value("feed")
  val Sleep = Value("sleep")
  val Clean = Value("clean")
  val Print = Value("print")
  val Reset = Value("reset")
  val Quit = Value("quit")

  def fromString(s: String): Option[Command] = {
    if (s.isEmpty) None
    else Command.values find (x => x.toString == s.toLowerCase || x.toString.startsWith(s.toLowerCase))
  }
}

