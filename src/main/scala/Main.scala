import akka.actor.{ActorSystem, Props}

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

object Main {

  def main(args: Array[String]): Unit = {

    val system = akka.actor.ActorSystem("system")
    val serviceActor = system.actorOf(Props[ServiceActor])
    system.scheduler.schedule(
      1.seconds,
      60.seconds + 1.nanosecond,
      serviceActor,
      Command.Print)

    Stream.continually(io.StdIn.readLine())
      .takeWhile(x => !isQuit(Option(x), system))
      .foreach { x =>
        Command.fromString(x) match {
          case None => println(s"commands: [${Command.values mkString ", "}] or just the first char")
          case Some(cmd) => serviceActor ! cmd
        }
      }
  }

  def isQuit(input: Option[String], system: ActorSystem): Boolean = {
    val quit = (input fold true)(x => Command.fromString(x) == Option(Command.Quit))
    if (quit) system.terminate()
    quit
  }
}
