import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

import akka.actor.Actor

class ServiceActor extends Actor {

  val bahamut = Bahamut()
  val step = 1
  var timestamp = LocalDateTime.now

  def receive = {
    case Command.Print => checkAndRun(())
    case Command.Feed  => checkAndRun(bahamut.feed())
    case Command.Sleep => checkAndRun(bahamut.sleep())
    case Command.Clean => checkAndRun(bahamut.clean())
    case Command.Reset =>
      bahamut.reset()
      timestamp = LocalDateTime.now
      println(bahamut)
  }

  private def checkAndRun(run: => Unit): Unit = {
    val now = LocalDateTime.now
    val ticks = ticksElapsed(timestamp, now, step)
    if (bahamut.isAlive) {
      if (ticks > 0) {
        bahamut.update(ticks, now.getMinute)
        timestamp = now
      }
      run
      println(s"time: $now")
      println(bahamut)
    } else println(s"$bahamut is dead, enter reset to restart game.")
  }

  private def ticksElapsed(from: LocalDateTime, to: LocalDateTime, step: Int) =
    ChronoUnit
      .MINUTES
      .between(from.truncatedTo(ChronoUnit.MINUTES), to.truncatedTo(ChronoUnit.MINUTES))
      .toInt / step
}
