import org.scalacheck.commands.Commands
import org.scalacheck.Gen
import org.scalacheck.Prop

import scala.util.{Random, Success, Try}
import Bahamut._

object BahamutCommands extends Commands {

  case class BahamutState(age: Int,
                          health: Int,
                          hunger: Int,
                          poop: Int,
                          activity: Activity)

  type State = BahamutState

  type Sut = Bahamut

  def canCreateNewSut(newState: State, initSuts: Traversable[State],
                      runningSuts: Traversable[Sut]): Boolean = true

  def initialPreCondition(state: State): Boolean = {
    state == BahamutState(0, Bahamut.MaxNum, Bahamut.limit, 0, Awake)
  }

  def newSut(state: State): Sut = new Bahamut(state.age, state.health, state.hunger, state.poop, state.activity)

  def destroySut(sut: Sut): Unit = ()

  def genInitialState: Gen[State] = Gen.const(BahamutState(0, Bahamut.MaxNum, Bahamut.limit, 0, Awake))

  def genCommand(state: State): Gen[Command] = Gen.oneOf(
    Update, Feed, Clean, Reset, Age, Health, Hunger, Poop, Activity, IsDead
  )

  case object Update extends UnitCommand {
    val ticks = Random.nextInt(48)
    val minute = Random.nextInt(60)
    def run(sut: Sut): Unit = sut.update(ticks, minute)
    def nextState(state: State): State = state.copy(
      age = state.age + ticks,
      health = clamp(state.health - starve(state.hunger, ticks) - sick(state.poop, ticks)),
      hunger = clamp(state.hunger + ticks),
      poop = clamp(state.poop + ticks),
      activity = if (minute % 3 == 0) Asleep else Awake
    )
    def preCondition(state: State): Boolean = true
    def postCondition(state: State, success: Boolean): Prop = success
  }

  case object Feed extends UnitCommand {
    def run(sut: Sut): Unit = sut.feed()
    def nextState(state: State): State = state.copy(
      health = clamp(state.health + 1),
      hunger = clamp(state.hunger - 1),
      activity = Awake
    )
    def preCondition(state: State): Boolean = true
    def postCondition(state: State, success: Boolean): Prop = success
  }

  case object Sleep extends UnitCommand {
    def run(sut: Sut): Unit = sut.sleep()
    def nextState(state: State): State = state.copy(activity = Asleep)
    def preCondition(state: State): Boolean = true
    def postCondition(state: State, success: Boolean): Prop = success
  }

  case object Clean extends UnitCommand {
    def run(sut: Sut): Unit = sut.clean()
    def nextState(state: State): State = state.copy(
      health = clamp(state.health + 1),
      poop = clamp(state.poop - 1)
    )
    def preCondition(state: State): Boolean = true
    def postCondition(state: State, success: Boolean): Prop = success
  }

  case object Reset extends UnitCommand {
    def run(sut: Sut): Unit = sut.reset()
    def nextState(state: State): State = BahamutState(0, Bahamut.MaxNum, Bahamut.limit, 0, Awake)
    def preCondition(state: State): Boolean = true
    def postCondition(state: State, success: Boolean): Prop = success
  }

  case object Age extends Command {
    type Result = Int
    def run(sut: Sut): Result = sut.age
    def nextState(state: State): State = state
    def preCondition(state: State): Boolean = true
    def postCondition(state: State, result: Try[Result]): Prop = result == Success(state).map(_.age/TicksPerAge)
  }

  case object Health extends Command {
    type Result = Int
    def run(sut: Sut): Result = sut.health
    def nextState(state: State): State = state
    def preCondition(state: State): Boolean = true
    def postCondition(state: State, result: Try[Result]): Prop = result == Success(state).map(_.health)
  }

  case object Hunger extends Command {
    type Result = Int
    def run(sut: Sut): Result = sut.hunger
    def nextState(state: State): State = state
    def preCondition(state: State): Boolean = true
    def postCondition(state: State, result: Try[Result]): Prop = result == Success(state).map(_.hunger)
  }

  case object Poop extends Command {
    type Result = Int
    def run(sut: Sut): Result = sut.poop
    def nextState(state: State): State = state
    def preCondition(state: State): Boolean = true
    def postCondition(state: State, result: Try[Result]): Prop = result == Success(state).map(_.poop)
  }

  case object Activity extends Command {
    type Result = Activity
    def run(sut: Sut): Result = sut.activity
    def nextState(state: State): State = state
    def preCondition(state: State): Boolean = true
    def postCondition(state: State, result: Try[Result]): Prop = result == Success(state).map(_.activity)
  }

  case object IsDead extends Command {
    type Result = Boolean
    def run(sut: Sut): Result = sut.isDead
    def nextState(state: State): State = state
    def preCondition(state: State): Boolean = true
    def postCondition(state: State, result: Try[Result]): Prop = {
      result == Success(state).map { s =>
        s.health <= 0 || s.age/TicksPerAge > MaxNum
      }
    }

  }
}
