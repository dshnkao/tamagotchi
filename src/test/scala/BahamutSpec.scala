import org.scalacheck._

object BahamutSpec extends Properties("Bahamut") {

  property("BahamutCommands") = BahamutCommands.property()

}
